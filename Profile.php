<?php

namespace westside\profile;

use Yii;
use yii\base\Module;

/**
 * profile module definition class
 */
class Profile extends Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'westside\profile\controllers';
    public $layout = "profile";

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->registerTranslations();
    }

    /**
     * Set translation params for module
     * @return void
     */
    public function registerTranslations()
    {
        Yii::$app->i18n->translations['profile'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en-US',
            'basePath' => '@vendor/westside/yii2-profile/messages',
        ];
    }
}
