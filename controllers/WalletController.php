<?php

namespace westside\profile\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Response;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use westside\profile\models\Wallets;
use westside\profile\models\search\WalletsSearch;

/**
 * Class WalletController
 * @package frontend\modules\profile\controllers
 */
class WalletController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'WalletAdd' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return Response
     */
    public function actionIndex()
    {
        $wallet = new Wallets();

        $searchModel = new WalletsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 8;
        $dataProvider->sort->defaultOrder = ['created_at' => SORT_DESC];

        return $this->render('index',
            compact('wallet', 'dataProvider', 'searchModel'));
    }

    /**
     * @return mixed
     */
    public function actionWalletAdd()
    {
        $payment = new Wallets();

        $payment->setAttributes([
            'user_id'           => Yii::$app->user->id,
            'payment_type_id'   => Wallets::PAYMENT_TYPE_INCOME,
            'payment_target_id' => Wallets::PAYMENT_TARGET_WALLET,
        ]);

        if ($payment->load(Yii::$app->request->post())
            && $payment->validate())
        {
            $payment->save();
        }

        return $this->redirect(URL::to(['/profile/wallet/index']));
    }
}
