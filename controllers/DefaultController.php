<?php

namespace westside\profile\controllers;

use Yii;
use common\models\Ad;
use yii\web\Controller;
use backend\models\search\AdSearch;

/**
 * Default controller for the `profile` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new AdSearch();
        $searchModel->user_id = Yii::$app->user->id;

        $searchModel->status = Ad::AD_STATUS_ACTIVE;
        $dataProviderActive = $searchModel
            ->search(Yii::$app->request->queryParams);

        $searchModel->status = Ad::AD_STATUS_INACTIVE;
        $dataProviderCompleted = $searchModel
            ->search(Yii::$app->request->queryParams);

        return $this->render('index',
            compact('searchModel', 'dataProviderActive', 'dataProviderCompleted'));
    }

    /**
     * @return string
     */
    public function actionNotReady()
    {
        return $this->render('not-ready');
    }

}
