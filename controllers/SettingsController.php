<?php

namespace westside\profile\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Response;
use yii\db\Expression;
use yii\web\Controller;
use westside\profile\models\User;
use westside\profile\models\UserDetails;
use vova07\fileapi\actions\UploadAction;

/**
 * Settings controller for the `profile` module
 */
class SettingsController extends Controller
{
    /**
     * @inheritdoc
     * @return array
     */
    public function actions()
    {
        return [
            'fileapi-upload' => [
                'class' => UploadAction::className(),
                'path' => '@frontend/web/content/users/temp'
            ]
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $userModel = User::find()
            ->where(['id' => Yii::$app->user->id])
            ->one();

        $userDetailsModel = $userModel->userDetails;

        if (!$userDetailsModel) {
            $userDetailsModel = new UserDetails();
        }

        if (Yii::$app->request->isPost)
        {
            $userDetailsModel->load(Yii::$app->request->post());

            if ($userDetailsModel->validate()) {
                $userModel->link('userDetails', $userDetailsModel);
            }
        }

        return $this->render('index', compact(
            'userDetailsModel', 'userModel'
        ));
    }

    /**
     * @return Response
     */
    public function actionChangePassword()
    {
        $userModel = User::find()
            ->where(Yii::$app->user->id)
            ->one();

        if ($userModel->load(Yii::$app->request->post())
            && $userModel->validate()) {
            $userModel->password_hash = Yii::$app->security
                ->generatePasswordHash($userModel->password_new);

            $userModel->save(false);
        }
        else {
            Yii::$app->session->setFlash('change_password', $userModel->getFirstError('password_old'));
        }

        return $this->redirect(Url::to(['/profile/settings/index']));
    }

    /**
     * @return Response
     */
    public function actionDeleteAccount()
    {
        $userModel = User::find()
            ->where(['id' => Yii::$app->user->id])
            ->one();

        $userModel->status = User::STATUS_ON_DELETE;
        $userModel->save(false);

        return $this->redirect(Url::to(['/profile/settings/index']));
    }

    /**
     * @return Response
     */
    public function actionRestoreAccount()
    {
        $userModel = User::find()
            ->where(['id' => Yii::$app->user->id])
            ->one();

        $userModel->status = User::STATUS_ACTIVE;
        $userModel->save(false);

        return $this->redirect(Url::to(['/profile/settings/index']));
    }

    /**
     * @return Response
     */
    public function actionDeleteAvatar()
    {
        $userDetailsModel = User::find()
            ->where(['id' => Yii::$app->user->id])
            ->one()
            ->userDetails;

        $userDetailsModel->detachBehavior('uploadBehavior');
        $userDetailsModel->setAttribute('avatar_url', new Expression('NULL'));
        $userDetailsModel->save(false, ['avatar_url']);

        return $this->redirect(Url::to(['/profile/settings/index']));
    }
}
