<?php

namespace westside\profile\controllers;

use Yii;
use yii\web\Response;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use westside\profile\helpers\UsersHelper as Users;
use westside\profile\models\Messages;
use westside\profile\models\search\MessagesSearch;
use westside\profile\models\Dialogs;
use westside\profile\models\search\DialogsSearch;
use westside\profile\models\MessageNotifications;
use westside\profile\actions\NotificationsAction;

/**
 * Class DialogsController
 * @package app\modules\profile\controllers
 */
class DialogsController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'MessageSend'      => ['post'],
                    'GetNotifications' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'notification-feed' => NotificationsAction::className()
        ];
    }

    /**
     * @return Response
     */
    public function actionIndex()
    {
        $messageModel = new Messages();
        $messageUsers = Users::getExcludeCurrent();

        $searchModel = new DialogsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 10;
        $dataProvider->query = Dialogs::find()
            ->where(['user_from_id' => Yii::$app->user->id])
            ->orWhere(['user_to_id' => Yii::$app->user->id])
            ->orderBy(['updated_at' => SORT_DESC]);

        return $this->render('index',
            compact('dataProvider', 'searchModel', 'messageModel', 'messageUsers'));
    }

    /**
     * @param $id integer
     * @return Response
     */
    public function actionMessages($id)
    {
        /** @var Dialogs $dialog */
        $dialog = $this->findDialogModel($id);

        if ($dialog->lastMessage->userTo->id == Yii::$app->user->id)
        {
            $dialog->status_id = Dialogs::DIALOG_STATUS_READ;
            $dialog->save();

            foreach ($dialog->getMessages()->all() as $message)
            {
                /** @var Messages $message */
                $message->status_id = Messages::MESSAGE_STATUS_READ;
                $message->save();

                /** @var MessageNotifications $notification */
                $notification = $message->getMessageNotification()->one();
                $notification->status_id = MessageNotifications::NOTIFICATION_STATUS_READ;
                $notification->save();
            }
        }

        $searchModel = new MessagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 5;
        $dataProvider->query = Messages::find()
            ->where(['dialog_id' => $dialog->id])
            ->orderBy(['created_at' => SORT_DESC]);

        $messageModel = new Messages();

        return $this->render('messages',
            compact('dataProvider', 'messageModel', 'dialog'));
    }

    /**
     * @param $id
     * @return Dialogs
     * @throws NotFoundHttpException
     */
    protected function findDialogModel($id)
    {
        if (($model = Dialogs::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @return Response
     */
    public function actionMessageSend()
    {
        $message = new Messages();
        $message->user_from_id = Yii::$app->user->id;
        $message->status_id    = Messages::MESSAGE_STATUS_NEW;
        $message->created_at   = time();
        $message->updated_at   = time();

        if ($message->load(Yii::$app->request->post()) && $message->validate())
        {
            $dialog = $this->getDialogByUsers(
                $message->user_from_id,
                $message->user_to_id
            );

            if ($dialog->validate())
            {
                $dialog->status_id = Dialogs::DIALOG_STATUS_NEW;
                $dialog->save();
                $dialog->link('messages', $message);
                $dialog->touch('updated_at');
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $from integer
     * @param $to integer
     * @return Dialogs|array|null
     */
    protected function getDialogByUsers($from, $to)
    {
        $dialog = Dialogs::find()
            ->andWhere(['in', 'user_from_id', [$from, $to]])
            ->andWhere(['in', 'user_to_id',   [$from, $to]])
            ->one();

        if (!$dialog)
        {
            $dialog = new Dialogs();
            $dialog->user_from_id = $from;
            $dialog->user_to_id   = $to;
            $dialog->created_at   = time();
        }

        return $dialog;
    }
}
