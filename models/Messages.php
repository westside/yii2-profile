<?php

namespace westside\profile\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use westside\profile\models\User as Users;
use westside\profile\models\query\MessagesQuery;

/**
 * This is the model class for table "messages".
 *
 * @property integer $id
 * @property integer $dialog_id
 * @property integer $user_to_id
 * @property integer $user_from_id
 * @property string  $message
 * @property integer $status_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property MessageAttachments[] $messageAttachments
 * @property MessageNotifications $messageNotification
 * @property Dialogs $dialog
 * @property Users   $userFrom
 * @property Users   $userTo
 */
class Messages extends ActiveRecord
{
    const MESSAGE_STATUS_NEW     = 0;
    const MESSAGE_STATUS_READ    = 1;
    const MESSAGE_STATUS_ARCHIVE = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'messages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_to_id', 'user_from_id', 'message'], 'required'],
            [['dialog_id', 'user_to_id', 'user_from_id', 'status_id', 'created_at', 'updated_at'], 'integer'],
            [['message'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => Yii::t('profile', 'ID'),
            'dialog_id'    => Yii::t('profile', 'Dialog ID'),
            'user_to_id'   => Yii::t('profile', 'User To ID'),
            'user_from_id' => Yii::t('profile', 'User From ID'),
            'message'      => Yii::t('profile', 'Message'),
            'status_id'    => Yii::t('profile', 'Status ID'),
            'created_at'   => Yii::t('profile', 'Created At'),
            'updated_at'   => Yii::t('profile', 'Updated At'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getMessageAttachments()
    {
        return $this->hasMany(MessageAttachments::className(), ['message_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getMessageNotification()
    {
        return $this->hasOne(MessageNotifications::className(), ['message_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDialog()
    {
        return $this->hasOne(Dialogs::className(), ['id' => 'dialog_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUserFrom()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_from_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUserTo()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_to_id']);
    }

    /**
     * @inheritdoc
     * @return MessagesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MessagesQuery(get_called_class());
    }

    /**
     * @return int|string
     */
    public static function getCountNewMessages()
    {
        return self::find()
            ->where([
                'user_to_id' => Yii::$app->user->id,
                'status_id'  => self::MESSAGE_STATUS_NEW,
            ])->count();
    }

    /**
     * @return array
     */
    public static function getMessagesStatuses()
    {
        return [
            self::MESSAGE_STATUS_NEW     => Yii::t('profile', 'MESSAGE_STATUS_NEW'),
            self::MESSAGE_STATUS_READ    => Yii::t('profile', 'MESSAGE_STATUS_READ'),
            self::MESSAGE_STATUS_ARCHIVE => Yii::t('profile', 'MESSAGE_STATUS_ARCHIVE'),
        ];
    }

    /**
     * @param $value
     * @return string|null
     */
    public static function getMessagesStatusText($value)
    {
        return ArrayHelper::getValue(self::getMessagesStatuses(), $value);
    }
}
