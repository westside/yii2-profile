<?php

namespace westside\profile\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use vova07\fileapi\behaviors\UploadBehavior;
use westside\profile\models\query\UserDetailsQuery;

/**
 * This is the model class for table "{{%user_details}}".
 *
 * @property integer $user_id
 * @property integer $location_id
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property integer $email_notifications
 * @property integer $event_notifications
 * @property string $avatar_url
 *
 * @property User $user
 */
class UserDetails extends ActiveRecord
{
    protected $phoneRegExp = '/^\+38\s\(0[\d]{2}\)\s[\d]{2}-[\d]{2}-[\d]{3}/';

    public $avatarPath = '@frontend/web/content/users/';
    public $avatarTemp = '@frontend/web/content/users/temp';
    public $avatarUrlPath = '/content/users';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_details}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'uploadBehavior' => [
                'class' => UploadBehavior::className(),
                'attributes' => [
                    'avatar_url' => [
                        'path' => $this->avatarPath,
                        'tempPath' => $this->avatarTemp,
                        'url' => $_SERVER['SERVER_NAME'].$this->avatarUrlPath,
                    ],
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'safe'],
            [['user_id', 'location_id', 'email_notifications', 'event_notifications'], 'integer'],
            [['first_name', 'last_name', 'phone'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['phone'], 'match', 'pattern' => $this->phoneRegExp],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('profile', 'User ID'),
            'location_id' => Yii::t('profile', 'Location ID'),
            'first_name' => Yii::t('profile', 'First Name'),
            'last_name' => Yii::t('profile', 'Last Name'),
            'phone' => Yii::t('profile', 'Phone'),
            'email_notifications' => Yii::t('profile', 'Email Notifications'),
            'event_notifications' => Yii::t('profile', 'Event Notifications'),
            'avatar_url' => Yii::t('profile', 'Avatar'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id'])->inverseOf('userDetails');
    }

    /**
     * @return string
     */
    public function getAvatarUrl()
    {
        return $this->avatarUrlPath.DIRECTORY_SEPARATOR.$this->avatar_url;
    }

    /**
     * @inheritdoc
     * @return UserDetailsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserDetailsQuery(get_called_class());
    }
}
