<?php

namespace westside\profile\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;
use westside\profile\models\User as Users;
use westside\profile\models\query\DialogsQuery;


/**
 * This is the model class for table "dialogs".
 *
 * @property integer $id
 * @property integer $user_to_id
 * @property integer $user_from_id
 * @property integer $dialog_type_id
 * @property string  $title
 * @property integer $status_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Users $userFrom
 * @property Users $userTo
 * @property Users $dialogCompanion
 * @property Messages[] $messages
 * @property Messages $lastMessage
 */
class Dialogs extends ActiveRecord
{
    const DIALOG_STATUS_NEW     = 0;
    const DIALOG_STATUS_READ    = 1;
    const DIALOG_STATUS_ARCHIVE = 2;

    const DIALOG_TYPE_PRIVATE = 0;
    const DIALOG_TYPE_SYSTEM = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dialogs';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_to_id', 'user_from_id'], 'required'],
            [['user_to_id', 'user_from_id', 'dialog_type_id', 'status_id'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'             => Yii::t('profile', 'ID'),
            'user_to_id'     => Yii::t('profile', 'User To ID'),
            'user_from_id'   => Yii::t('profile', 'User From ID'),
            'dialog_type_id' => Yii::t('profile', 'Dialog Type ID'),
            'title'          => Yii::t('profile', 'Title'),
            'status_id'      => Yii::t('profile', 'Status ID'),
            'created_at'     => Yii::t('profile', 'Created At'),
            'updated_at'     => Yii::t('profile', 'Updated At'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getUserFrom()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_from_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUserTo()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_to_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Messages::className(), ['dialog_id' => 'id']);
    }



    /**
     * @return array|null|ActiveRecord
     */
    public function getLastMessage()
    {
        return $this->getMessages()
            ->orderBy(['created_at' => SORT_DESC])
            ->one();
    }

    /**
     * @return Users
     */
    public function getDialogCompanion()
    {
        if ($this->userFrom->id != Yii::$app->user->id){
            return $this->userFrom;
        } else {
            return $this->userTo;
        }
    }

    /**
     * @inheritdoc
     * @return DialogsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DialogsQuery(get_called_class());
    }

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return [
            self::DIALOG_STATUS_NEW     => Yii::t('profile', 'DIALOG_STATUS_NEW'),
            self::DIALOG_STATUS_READ    => Yii::t('profile', 'DIALOG_STATUS_READ'),
            self::DIALOG_STATUS_ARCHIVE => Yii::t('profile', 'DIALOG_STATUS_ARCHIVE'),
        ];
    }

    /**
     * @param $value
     * @return string|null
     */
    public static function getStatusText($value)
    {
        return ArrayHelper::getValue(self::getStatuses(), $value);
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::DIALOG_TYPE_PRIVATE => Yii::t('profile', 'DIALOG_TYPE_PRIVATE'),
            self::DIALOG_TYPE_SYSTEM  => Yii::t('profile', 'DIALOG_TYPE_SYSTEM'),
        ];
    }

    /**
     * @param $value
     * @return string|null
     */
    public static function getTypeText($value)
    {
        return ArrayHelper::getValue(self::getTypes(), $value);
    }
}
