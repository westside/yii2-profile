<?php
namespace westside\profile\models;

use Yii;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use common\models\User as YiiUser;
use westside\profile\models\query\UserQuery;

/**
 * User model
 *
 * @property integer $id
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 *
 * @property string $password_old
 * @property string $password_new
 * @property string $password_repeat
 *
 * @property UserDetails $userDetails
 */

class User extends YiiUser
{
    const STATUS_NEW     = 1;
    const STATUS_BLOCKED = 2;

    public $password_old;
    public $password_new;
    public $password_repeat;

    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [
            ['password_old', 'trim'],
            ['password_old', 'required'],
            ['password_old', 'checkPassword'],

            ['password_new', 'trim'],
            ['password_new', 'required'],
            ['password_new', 'string', 'min' => 6, 'max' => 255],

            ['password_repeat', 'trim'],
            ['password_repeat', 'required'],
            ['password_repeat', 'compare', 'compareAttribute' => 'password_new']
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'password_old'    => Yii::t('profile', 'Password Old'),
            'password_new'    => Yii::t('profile', 'Password New'),
            'password_repeat' => Yii::t('profile', 'Password Repeat'),
        ]);
    }

    /**
     * @return ActiveQuery
     */
    public function getUserDetails()
    {
        return $this->hasOne(UserDetails::className(), ['user_id' => 'id'])->inverseOf('user');
    }

    /**
     * @inheritdoc
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function checkPassword($attribute, $params)
    {
        if (!Yii::$app->getSecurity()
            ->validatePassword($this->password_old, Yii::$app->user->identity->password_hash)
        )
            $this->addError($attribute, Yii::t('profile', '{password_old} is incorrect', [
                'password_old' => Yii::t('profile', 'Password Old')
            ]));
    }
}
