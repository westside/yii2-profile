<?php

namespace westside\profile\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;
use westside\profile\models\User as Users;
use westside\profile\models\query\WalletsQuery;

/**
 * This is the model class for table "wallets".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $shop_id
 * @property integer $payment_type_id
 * @property integer $payment_target_id
 * @property integer $payment_system_id
 * @property string  $payment_value
 * @property string  $description
 * @property integer $created_at
 *
 * @property Users  $user
 * @property string $userEmail
 */
class Wallets extends ActiveRecord
{
    /** Minimal income amount */
    const PAYMENT_MINIMAL_VALUE = 100;

    /** Payment types */
    const PAYMENT_TYPE_INCOME  = 0;
    const PAYMENT_TYPE_OUTCOME = 1;

    /** Payment targets */
    const PAYMENT_TARGET_WALLET = 0;
    const PAYMENT_TARGET_TOP    = 1;

    /** Payment systems */
    const PAYMENT_SYSTEM_CARD          = 0;
    const PAYMENT_SYSTEM_SMS           = 1;
    const PAYMENT_SYSTEM_YANDEX        = 2;
    const PAYMENT_SYSTEM_WEBMONEY      = 3;
    const PAYMENT_SYSTEM_QIWI_TERMINAL = 4;
    const PAYMENT_SYSTEM_QIWI_WALLET   = 5;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%wallets}}';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'payment_type_id', 'payment_target_id', 'payment_system_id', 'payment_value'], 'required'],
            [['user_id', 'shop_id', 'payment_type_id', 'payment_target_id', 'payment_system_id'], 'integer'],
            [['payment_value'], 'number'],
            [['payment_value'], 'compare', 'compareValue' => self::PAYMENT_MINIMAL_VALUE, 'operator' => '>='],
            [['description'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                => Yii::t('profile', 'ID'),
            'user_id'           => Yii::t('profile', 'User'),
            'shop_id'           => Yii::t('profile', 'Shop'),
            'payment_type_id'   => Yii::t('profile', 'Payment Type'),
            'payment_target_id' => Yii::t('profile', 'Payment Target'),
            'payment_system_id' => Yii::t('profile', 'Payment System'),
            'payment_value'     => Yii::t('profile', 'Payment Value'),
            'description'       => Yii::t('profile', 'Payment Description'),
            'created_at'        => Yii::t('profile', 'Created At'),
            'userEmail'         => Yii::t('profile', 'User Email'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @return string
     */
    public function getUserEmail()
    {
        return $this->user->email;
    }

    /**
     * @inheritdoc
     * @return WalletsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new WalletsQuery(get_called_class());
    }

    /**
     * @return array
     */
    public static function getPaymentTypes()
    {
        return [
            self::PAYMENT_TYPE_INCOME  => Yii::t('profile', 'PAYMENT_TYPE_INCOME'),
            self::PAYMENT_TYPE_OUTCOME => Yii::t('profile', 'PAYMENT_TYPE_OUTCOME'),
        ];
    }

    /**
     * @param $value integer
     * @return string
     */
    public static function getPaymentTypesText($value)
    {
        return ArrayHelper::getValue(self::getPaymentTypes(), $value);
    }

    /**
     * @return array
     */
    public static function getPaymentTargets()
    {
        return [
            self::PAYMENT_TARGET_TOP    => Yii::t('profile', 'PAYMENT_TARGET_TOP'),
            self::PAYMENT_TARGET_WALLET => Yii::t('profile', 'PAYMENT_TARGET_WALLET'),
        ];
    }

    /**
     * @param $value integer
     * @return string
     */
    public static function getPaymentTargetsText($value)
    {
        return ArrayHelper::getValue(self::getPaymentTargets(), $value);
    }

    /**
     * @return array
     */
    public static function getPaymentSystems()
    {
        return [
            self::PAYMENT_SYSTEM_CARD          => Yii::t('profile', 'PAYMENT_SYSTEM_CARD'),
            self::PAYMENT_SYSTEM_SMS           => Yii::t('profile', 'PAYMENT_SYSTEM_SMS'),
            self::PAYMENT_SYSTEM_YANDEX        => Yii::t('profile', 'PAYMENT_SYSTEM_YANDEX'),
            self::PAYMENT_SYSTEM_WEBMONEY      => Yii::t('profile', 'PAYMENT_SYSTEM_WEBMONEY'),
            self::PAYMENT_SYSTEM_QIWI_WALLET   => Yii::t('profile', 'PAYMENT_SYSTEM_QIWI_WALLET'),
            self::PAYMENT_SYSTEM_QIWI_TERMINAL => Yii::t('profile', 'PAYMENT_SYSTEM_QIWI_TERMINAL'),
        ];
    }

    /**
     * @param $value integer
     * @return string
     */
    public static function getPaymentSystemText($value)
    {
        return ArrayHelper::getValue(self::getPaymentSystems(), $value);
    }

    /**
     * @return float
     */
    public static function getUserBalance()
    {
        $income = self::find()->where([
                'payment_type_id' => self::PAYMENT_TYPE_INCOME,
                'user_id'         => Yii::$app->user->id,
            ])
            ->sum('payment_value');

        $outcome =  self::find()->where([
                'payment_type_id' => self::PAYMENT_TYPE_OUTCOME,
                'user_id'         => Yii::$app->user->id,
            ])
            ->sum('payment_value');

        return $income - $outcome;

        /*return Yii::$app->formatter
            ->asDecimal(($income - $outcome), 2);*/
    }
}
