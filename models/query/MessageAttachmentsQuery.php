<?php

namespace westside\profile\models\query;

use yii\db\ActiveQuery;
use westside\profile\models\MessageAttachments;

/**
 * This is the ActiveQuery class for [[MessageAttachments]].
 *
 * @see MessageAttachments
 */
class MessageAttachmentsQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return MessageAttachments[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MessageAttachments|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}