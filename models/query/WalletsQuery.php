<?php

namespace westside\profile\models\query;

use yii\db\ActiveQuery;
use westside\profile\models\Wallets;

/**
 * This is the ActiveQuery class for [[Wallets]].
 *
 * @see Wallets
 */
class WalletsQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return Wallets[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Wallets|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}