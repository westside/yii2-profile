<?php

namespace westside\profile\models\query;

use yii\db\ActiveQuery;
use westside\profile\models\UserDetails;

/**
 * This is the ActiveQuery class for [[frontend\modules\profile\models\UserDetails]].
 *
 * @see UserDetails
 */
class UserDetailsQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return UserDetails[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UserDetails|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
