<?php

namespace westside\profile\models\query;

use yii\db\ActiveQuery;
use westside\profile\models\Dialogs;

/**
 * This is the ActiveQuery class for [[Dialogs]].
 *
 * @see Dialogs
 */
class DialogsQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return Dialogs[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Dialogs|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}