<?php

namespace westside\profile\models\query;

use yii\db\ActiveQuery;
use westside\profile\models\MessageNotifications;

/**
 * This is the ActiveQuery class for [[MessageNotifications]].
 *
 * @see MessageNotifications
 */
class MessageNotificationsQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return MessageNotifications[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MessageNotifications|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}