<?php

namespace westside\profile\models\query;

use yii\db\ActiveQuery;
use westside\profile\models\User;

/**
 * This is the ActiveQuery class for [[\frontend\modules\profile\modules\User]].
 *
 * @see User
 */
class UserQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return User[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return User|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
