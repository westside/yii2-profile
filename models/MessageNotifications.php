<?php

namespace westside\profile\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;
use westside\profile\models\User as Users;
use westside\profile\models\query\MessageNotificationsQuery;

/**
 * This is the model class for table "notifications".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $message_id
 * @property integer $type_id
 * @property string $text
 * @property integer $status_id
 * @property integer $created_at
 *
 * @property Users $user
 * @property Messages $message
 */
class MessageNotifications extends ActiveRecord
{
    const NOTIFICATION_TYPE_PM = 0;
    const NOTIFICATION_TYPE_SM = 1;

    const NOTIFICATION_STATUS_NEW  = 0;
    const NOTIFICATION_STATUS_READ = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message_notifications';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'message_id', 'text'], 'required'],
            [['user_id', 'message_id', 'type_id', 'status_id'], 'integer'],
            [['text'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('profile', 'ID'),
            'user_id'    => Yii::t('profile', 'User ID'),
            'message_id' => Yii::t('profile', 'Message ID'),
            'type_id'    => Yii::t('profile', 'Type ID'),
            'text'       => Yii::t('profile', 'Text'),
            'status_id'  => Yii::t('profile', 'Status ID'),
            'created_at' => Yii::t('profile', 'Created At'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getMessage()
    {
        return $this->hasOne(Messages::className(), ['id' => 'message_id']);
    }

    /**
     * @inheritdoc
     * @return MessageNotificationsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MessageNotificationsQuery(get_called_class());
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::NOTIFICATION_TYPE_PM => Yii::t('profile', 'NOTIFICATION_TYPE_PM'),
            self::NOTIFICATION_TYPE_SM => Yii::t('profile', 'NOTIFICATION_TYPE_SM'),
        ];
    }

    /**
     * @param $value integer
     * @return string
     */
    public static function getTypeText($value)
    {
        return ArrayHelper::getValue(self::getTypes(), $value);
    }

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return [
            self::NOTIFICATION_STATUS_NEW  => Yii::t('profile', 'NOTIFICATION_STATUS_NEW'),
            self::NOTIFICATION_STATUS_READ => Yii::t('profile', 'NOTIFICATION_STATUS_READ'),
        ];
    }

    /**
     * @param $value integer
     * @return string
     */
    public static function getStatusText($value)
    {
        return ArrayHelper::getValue(self::getStatuses(), $value);
    }
}
