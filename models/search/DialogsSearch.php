<?php

namespace westside\profile\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use westside\profile\models\Dialogs;

/**
 * DialogsSearch represents the model behind the search form about `app\models\Dialogs`.
 */
class DialogsSearch extends Dialogs
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_to_id', 'user_from_id', 'dialog_type_id', 'status_id', 'created_at'], 'integer'],
            [['title'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Dialogs::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_to_id' => $this->user_to_id,
            'user_from_id' => $this->user_from_id,
            'dialog_type_id' => $this->dialog_type_id,
            'status_id' => $this->status_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}
