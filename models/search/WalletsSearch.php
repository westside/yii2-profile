<?php

namespace westside\profile\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use westside\profile\models\Wallets;

/**
 * WalletsSearch represents the model behind the search form about `app\models\Wallets`.
 */
class WalletsSearch extends Wallets
{
    public $userEmail;

    public $downPrice;
    public $upPrice;

    public function init()
    {
        parent::init();

        $this->downPrice = $this->find()->min('payment_value');
        $this->upPrice = $this->find()->max('payment_value');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'shop_id', 'payment_type_id', 'payment_target_id', 'payment_system_id', 'created_at'], 'integer'],
            [['payment_value'], 'number'],
            [['description', 'userEmail'], 'safe'],
            [['downPrice', 'upPrice'], 'number'],
            /*[['downPrice', 'upPrice'], 'filter', 'filter' => function($value) {
                return Yii::$app->formatter->asDecimal($value, 2, null, ['thousandSeparator' => null]);
            }],*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Wallets::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['userEmail'] = [
            'asc' => ['users.email' => SORT_ASC],
            'desc' => ['users.email' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            $query->joinWith(['user']);
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'shop_id' => $this->shop_id,
            'payment_type_id' => $this->payment_type_id,
            'payment_target_id' => $this->payment_target_id,
            'payment_system_id' => $this->payment_system_id,
            'payment_value' => $this->payment_value,
            'created_at' => $this->created_at,
        ]);


        $query->andFilterWhere(['>=', 'payment_value', $this->downPrice]);
        $query->andFilterWhere(['<=', 'payment_value', $this->upPrice]);

        $query->joinWith(['user' => function ($subquery) {
            $subquery->andFilterWhere(['like', 'users.email', $this->userEmail]);
        }]);

        $query->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
