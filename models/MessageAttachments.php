<?php

namespace westside\profile\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use westside\profile\models\query\MessageAttachmentsQuery;

/**
 * This is the model class for table "message_attachments".
 *
 * @property integer $id
 * @property integer $message_id
 * @property string $url
 * @property string $title
 * @property integer $type_id
 *
 * @property Messages $message
 */
class MessageAttachments extends ActiveRecord
{
    /**
     * @var UploadedFile;
     */
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message_attachments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message_id', 'url'], 'required'],
            [['message_id', 'type_id'], 'integer'],
            [['url', 'title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('profile', 'ID'),
            'message_id' => Yii::t('profile', 'Message ID'),
            'url'        => Yii::t('profile', 'Url'),
            'title'      => Yii::t('profile', 'Title'),
            'type_id'    => Yii::t('profile', 'Type ID'),
            'file'       => Yii::t('profile', 'Attachments'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getMessage()
    {
        return $this->hasOne(Messages::className(), ['id' => 'message_id']);
    }

    /**
     * @inheritdoc
     * @return MessageAttachmentsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MessageAttachmentsQuery(get_called_class());
    }
}
