<?php

return [
    'Dialogs' => 'Диалоги',
    'Dialog with' => 'Диалог c пользователем',
    'Subject' => 'Тема',
    'From' => 'От',
    'Created At' => 'Отправлено',
    'At' => 'в',
    'Read More' => 'Просмотреть',
    'Write New Message' => 'Написать сообщение',
    'Message' => 'Текст сообщения',
    'Undefined' => 'Не выбрано',
    'Send' => 'Отправить сообщение',
    'New Message' => 'Новое сообщение',
    'User To ID' => 'Получатель',
];