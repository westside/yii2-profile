<?php

return [
    /* Dialogs translates */
    'Dialogs' => 'Диалоги',
    'Dialog with' => 'Диалог c пользователем',
    'Subject' => 'Тема',
    'From' => 'От',
    'Created At' => 'Отправлено',
    'At' => 'в',
    'Read More' => 'Просмотреть',
    'Write New Message' => 'Написать сообщение',
    'Message' => 'Текст сообщения',
    'Undefined' => 'Не выбрано',
    'Send' => 'Отправить сообщение',
    'New Message' => 'Новое сообщение',
    'User To ID' => 'Получатель',
    /* End of Dialogs translates */

    /* Layout translates */
    'Search In Region' => 'Поиск в регионе',
    'All Regions' => 'Все регионы',
    'All ads' => 'Все объявления',
    'Shops' => 'Магазины',
    'Support' => 'Служба поддержки',
    'Site Map' => 'Карта Сайта',
    'Default Value' => 'Не выбрано',
    'Search Ads' => 'Поиск объявлений',
    'Search' => 'Поиск',
    'Events List' => 'Лента событий',
    'My Ads' => 'Мои объявления',
    'Selected Ads' => 'Избранные объявления',
    'Friends' => 'Друзья',
    'Messages' => 'Сообщения',
    'Wallet' => 'Кошелек',
    'UAH' => 'грн',
    'Media' => 'Медиа',
    'Documents' => 'Документы',
    'Communities' => 'Сообщества',
    'Settings' => 'Настройки',
    'Exit' => 'Выход',
    /* End of Layout translates*/

    /* Ads list translates */
    'Active' => 'Активные',
    'Completed' => 'Завершенные',
    'Select All' => 'Выбрать все',
    'Order' => 'Сортировать',
    'byDate' => 'по дате',
    'byView' => 'по просмотрам',
    'byRating' => 'по рейтингу',
    'Ad Number' => 'Номер объявления',
    'Published At' => 'Размещено',
    'Days Left' => 'Осталось',
    'View' => 'Просмотреть',
    'Edit' => 'Редактировать',
    'Cancel' => 'Деактивировать',
    'Advertise' => 'Рекламировать',
    'Statistic' => 'Статистика',
    'Views' => 'Просмотров',
    'Phone' => 'Телефон',
    'In Favourites' => 'В избранных',
    /* End of Ads list translates */
];