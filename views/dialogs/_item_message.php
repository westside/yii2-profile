<?php

use yii\helpers\Url;

/** @var $this  \yii\web\View */
/** @var $model \frontend\modules\profile\models\Messages */

$readMoreLink = Url::to(['/profile/dialogs/messages', 'id' => $model->dialog->id]);
$userFromDetails = $model->userFrom->userDetails;
if ($userFromDetails) {
    if ($userFromDetails->avatar_url)
        $userAvatar = $userFromDetails->getAvatarUrl();
}
else $userAvatar   = implode('', [
    'http://dummyimage.com/60x60/555/fff&text=',
    $model->userFrom->email,
]);
?>

<?php if ($model->userFrom->id != Yii::$app->user->id): ?>

    <div class="media-left">
        <a href="<?= $readMoreLink ?>">
            <img class="media-object img-thumbnail" src="<?= $userAvatar ?>" style="width: 80px; height: 80px">
        </a>
    </div>
    <div class="media-body">
        <blockquote>
            <p><?= $model->message ?></p>
            <footer>
                <?= Yii::t('profile', 'From') ?>: <?= $model->userFrom->email ?>
                <?= Yii::t('profile', 'At') ?>: <?= Yii::$app->formatter->asDatetime($model->created_at) ?>
            </footer>
        </blockquote>
    </div>

<?php else: ?>

    <div class="media-body">
        <blockquote class="blockquote-reverse">
            <p><?= $model->message?></p>
            <footer>
                <?= Yii::t('profile', 'From') ?>: <?= $model->userFrom->email ?>
                <?= Yii::t('profile', 'At') ?>: <?= Yii::$app->formatter->asDatetime($model->created_at) ?>
            </footer>
        </blockquote>
    </div>
    <div class="media-right">
        <a href="<?= $readMoreLink ?>">
            <img class="media-object img-thumbnail" src="<?= $userAvatar ?>" style="width: 80px; height: 80px">
        </a>
    </div>

<?php endif; ?>

