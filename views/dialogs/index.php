<?php

use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\widgets\ListView;
use yii\bootstrap\ActiveForm;

/** @var $this         \yii\web\View */
/** @var $messageUsers array */
/** @var $messageModel \frontend\modules\profile\models\Messages */
/** @var $dataProvider \yii\data\ActiveDataProvider */

/*$this->title = Yii::t('profile', 'Dialogs');
$this->params['breadcrumbs'][] = $this->title;*/

$formSendMessageOptions = [
    'action' => Url::to(['/profile/dialogs/message-send'])
];
$formDropDownListOptions = [
    'prompt' => Yii::t('profile', 'Undefined')
];
$modalSendMessageOptions = [
    'id'           => 'modalSendMessage',
    'options'      => ['class' => 'text-left'],
    'header'       => '<h4>' . Yii::t('profile', 'Write New Message') . '</h4>',
    'toggleButton' => [
        'tag'   => 'a',
        'label' => Yii::t('profile', 'Write New Message'),
        'class' => 'btn btn-default',
    ],
];
$dialogsListOptions = [
    'dataProvider' => $dataProvider,
    'itemView'     => function($model, $key, $index, $widget){
        $model = $model->lastMessage;
        return $this->render('_item_dialog', compact('model'));
    },
    'options'     => [
        'tag'   => 'section',
        'id'    => false,
        'class' => 'messages-box',
    ],
    'layout'      => "{items}\n{pager}",
    'itemOptions' => [
        'tag'   => 'div',
        'class' => 'messages-block',
    ],
];
?>

<?php $this->registerJs('
    setInterval(function(){
        if (!$(\'#modalSendMessage\').hasClass(\'in\')){
            $.pjax.reload({container:\'#dialogsList\'});
        }
    }, 3000);
'); ?>

<main class="content-holder">
    <section class="messages-section">
        <ul class="nav nav-tabs messages" role="tablist">
            <li role="presentation" class="active"><a href="#m1" aria-controls="m1" role="tab" data-toggle="tab"><i class="fa fa-exchange"></i>Приватні</a></li>
            <li role="presentation" class=""><a href="#m2" aria-controls="m2" role="tab" data-toggle="tab"><i class="fa fa-cog"></i>Системні</a></li>
            <li role="presentation" class=""><a href="#m3" aria-controls="m3" role="tab" data-toggle="tab"><i class="fa fa-archive"></i>Архів</a></li>
            <div class="send-message-box text-right" style="margin-bottom: 0">
                <?php Modal::begin($modalSendMessageOptions); ?>
                    <?php $form = ActiveForm::begin($formSendMessageOptions); ?>
                        <?= $form->field($messageModel, 'user_to_id')->dropDownList($messageUsers, $formDropDownListOptions) ?>
                        <?= $form->field($messageModel, 'message')->textarea(['rows' => '4']) ?>
                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('profile', 'Send'), ['class' => 'btn btn-green-gradient']) ?>
                        </div>
                    <?php ActiveForm::end(); ?>
                <?php Modal::end(); ?>
            </div>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="m1">
                <? Pjax::begin(['id' => 'dialogsList'])?>
                    <?= ListView::widget($dialogsListOptions)?>
                <?php Pjax::end() ?>
            </div>
            <div role="tabpanel" class="tab-pane" id="m2"></div>
            <div role="tabpanel" class="tab-pane" id="m3"></div>
        </div>
    </section>
</main>