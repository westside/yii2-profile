<?php

use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\widgets\ListView;
use yii\bootstrap\ActiveForm;

/** @var $this            \yii\web\View */
/** @var $dialog          \frontend\modules\profile\models\Dialogs */
/** @var $messageModel    \frontend\modules\profile\models\Messages */
/** @var $dataProvider    \yii\data\ActiveDataProvider */
/** @var $searchModel     \frontend\modules\profile\models\search\DialogsSearchl */

$this->title = Yii::t('profile', 'Dialog with') . ': ' . $dialog->dialogCompanion->email;
$this->params['breadcrumbs'][] = ['label' => Yii::t('profile', 'Dialogs'), 'url' => ['/profile/dialogs/index']];
$this->params['breadcrumbs'][] = $this->title;

$formSendMessageOptions = [
    'action' => Url::to(['/profile/dialogs/message-send'])
];

$formDropDownListOptions = [
    'prompt' => Yii::t('profile', 'UNDEFINED')
];

$modalSendMessageOptions = [
    'id'           => 'modalSendMessage',
    'header'       => '<h4>' . Yii::t('profile', 'Write New Message') . '</h4>',
    'toggleButton' => [
        'label' => Yii::t('profile', 'Write new message'),
        'class' => 'btn btn-primary pull-right',
    ],
];

$messagesListOptions = [
    'dataProvider' => $dataProvider,
    'itemView'     => function($model, $key, $index, $widget){
        return $this->render('_item_message', compact('model'));
    },
    'pager'       => [
        'options' => [
            'class' => 'pagination center-block'
        ],
    ],
    'layout'      => "{items}\n{pager}",
    'itemOptions' => [
        'tag'   => 'div',
        'class' => 'media',
    ],
];
?>

<div class="row" style="padding-bottom: 20px">
    <div class="col-sm-12">
        <?php Modal::begin($modalSendMessageOptions); ?>
        <?php $form = ActiveForm::begin($formSendMessageOptions); ?>
            <?= Html::activeHiddenInput($messageModel, 'user_to_id', ['value' => $dialog->dialogCompanion->id]) ?>
            <?= $form->field($messageModel, 'message')->textarea(['rows' => '4']) ?>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('profile', 'Send  '), ['class' => 'btn btn-primary']) ?>
            </div>
        <?php ActiveForm::end(); ?>
        <?php Modal::end(); ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <? Pjax::begin(['id' => 'messagesList'])?>
            <?= ListView::widget($messagesListOptions)?>
        <?php Pjax::end() ?>
    </div>
</div>
<?php $this->registerJs('
    setInterval(function(){
        if (!$(\'#modalSendMessage\').hasClass(\'in\')){
            $.pjax.reload({container:\'#messagesList\'});
        }
    }, 3000);
'); ?>
