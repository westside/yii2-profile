<?php

use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\helpers\StringHelper;
use westside\profile\models\Dialogs;
use westside\profile\models\Messages;

/** @var $model \frontend\modules\profile\models\Messages */

$newMessages      = null;
$newMessagesCount = null;
if ($model->dialog->status_id == Dialogs::DIALOG_STATUS_NEW)
    if ($model->userTo->id == Yii::$app->user->id)
    {
        $newMessages = 'new-messages';
        $newMessagesCount = $model->dialog
            ->getMessages()
            ->where(['user_to'   => Yii::$app->user->id ])
            ->where(['status_id' => Messages::MESSAGE_STATUS_NEW])
            ->count();
    }

$readMoreLink = Url::to(['/profile/dialogs/messages', 'id' => $model->dialog->id]);
$dialogCompanionDetails = $model->dialog->dialogCompanion->userDetails;
if ($dialogCompanionDetails) {
    if ($dialogCompanionDetails->avatar_url)
        $userAvatar = $dialogCompanionDetails->getAvatarUrl();
}
else $userAvatar   = implode('', [
    'http://dummyimage.com/60x60/555/fff&text=',
    $model->userFrom->email,
]);
?>

<div class="media-left">
    <a href="<?= $readMoreLink ?>">
        <img class="media-object img-thumbnail" src="<?= $userAvatar ?>" style="width: 80px; height: 80px">
        <?php if ($newMessages): ?>
            <span class="badge"><?= $newMessagesCount ?></span>
            <span class="label label-success col-sm-12">NEW</span>
        <?php endif; ?>
    </a>
</div>
<div class="media-body <?= $newMessages ?>">
    <?php if ($model->dialog->title): ?>
        <h3 class="media-heading text-muted">
            <?= Yii::t('profile', 'Subject') ?>: <?= $model->dialog->title ?>
        </h3>
    <?php endif ?>
    <blockquote>
        <p><?= StringHelper::truncateWords($model->message, 25) ?></p>
        <footer>
            <?= Yii::t('profile', 'From') ?>: <?= $model->userFrom->email ?>
            <?= Yii::t('profile', 'At') ?>: <?= Yii::$app->formatter->asDatetime($model->created_at) ?>
        </footer>
    </blockquote>
    <?= Html::a(Yii::t('profile', 'Read More'), $readMoreLink, ['class' => 'btn btn-primary pull-right']) ?>
</div>