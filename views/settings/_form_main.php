<section class="descr-settings">
    <div class="top">
        <div class="row">
            <div class="col-xs-3">
                <p>E-mail</p>
            </div>
            <div class="col-xs-6">
                <p><input type="text" class="ghost-input" placeholder="200419922011@mail.ru" required> </p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-3">
                <p>Номер користувача</p>
            </div>
            <div class="col-xs-6">
                <p><input type="text" class="ghost-input" placeholder="45768486" required> </p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-3">
                <p>Тип користувача</p>
            </div>
            <div class="col-xs-6">
                <p><input type="text" class="ghost-input" placeholder="Приватна особа" required> </p>
            </div>
        </div>
    </div>
    <div class="bottom">
        <div class="row">
            <div class="col-xs-3">
                <p>Оголошення</p>
            </div>
            <div class="col-xs-9">
                <p>4 (з них 2 активних) <a href="#">перейти до списку оголошень</a></p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-3">
                <p>Гаманець</p>
            </div>
            <div class="col-xs-9">
                <p>100 грн. <a href="#">перейти в гаманець</a></p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-3">
                <p>Магазин</p>
            </div>
            <div class="col-xs-9">
                <p><a href="#">Відкрити магазин</a></p>
            </div>
        </div>
    </div>
</section>