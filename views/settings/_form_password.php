<?php

use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;

/* @var $this \yii\web\View */
/* @var $userModel \frontend\modules\profile\models\User */
?>

<?php /*\yii\widgets\Pjax::begin() */?><!--
<?php /*$form = ActiveForm::begin([
    'layout' => 'horizontal',
    'action' => Url::to(['/profile/settings/change-password']),
]) */?>

    <?php /*if(Yii::$app->session->hasFlash('change_password')): */?>
        <div class="form-group">
            <div class="col-sm-12">
                <div class="alert alert-danger" role="alert">
                    <?/*= Yii::$app->session->getFlash('change_password') */?>
                </div>
            </div>
        </div>
    <?php /*endif; */?>

    <?/*= $form->field($userModel, 'password_old')->passwordInput() */?>
    <?/*= $form->field($userModel, 'password_new')->passwordInput() */?>
    <?/*= $form->field($userModel, 'password_repeat')->passwordInput() */?>

    <div class="form-group">
        <div class="col-sm-6 col-sm-offset-3">
            <?/*= Html::submitButton(Yii::t('profile', 'Change Password'), ['class' => 'btn btn-primary']) */?>
        </div>
    </div>

<?php /*$form->end() */?>
--><?php /*\yii\widgets\Pjax::end() */?>

<section class="settings-panel">
    <div class="head">
        <div class="row">
            <div class="col-xs-6">
                Зміна пароля
            </div>
            <div class="col-xs-6 text-right">
                <a class="" role="button" data-toggle="collapse" href="#collapse2" aria-expanded="false" aria-controls="collapse2"></a>
            </div>
        </div>
    </div>
    <div class="panel-box">
        <div class="collapse" id="collapse2">
            <div class="well">
                <div class="row line">
                    <div class="col-xs-12">
                        <p class="desc-panel">Введіть свій поточний пароль, новий пароль, і повторіть введення нового пароля, щоб виключити можливість помилки.</p>
                    </div>
                </div>
                <div class="row line">
                    <div class="col-xs-3">
                        <p>Поточний пароль</p>
                    </div>
                    <div class="col-xs-5">
                        <input class="form-control" value="">
                        <div class="help-block help-block-error"></div>
                    </div>
                </div>
                <div class="row line">
                    <div class="col-xs-3">
                        <p>Новий пароль</p>
                    </div>
                    <div class="col-xs-5">
                        <input class="form-control" value="">
                        <div class="help-block help-block-error"></div>
                    </div>
                </div>
                <div class="row line">
                    <div class="col-xs-3">
                        <p>Повторіть пароль</p>
                    </div>
                    <div class="col-xs-5">
                        <input class="form-control" value="">
                        <div class="help-block help-block-error"></div>
                    </div>
                </div>
                <div class="row line">
                    <div class="col-xs-3">
                        <p></p>
                    </div>
                    <div class="col-xs-4">
                        <p><a class="btn btn-green" href="#">Змінити пароль</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
