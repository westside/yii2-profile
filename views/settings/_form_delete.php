<?php

/* @var $this \yii\web\View */

use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use westside\profile\models\User;
?>

<?php /*if (Yii::$app->user->identity->status !== User::STATUS_DELETED): */?><!--

    <?php /*$form = ActiveForm::begin([
        'action' => Url::to(['/profile/settings/delete-account'])
    ]); */?>
        <?/*= Html::submitButton(Yii::t('profile', 'Delete Account'), [
            'class' => 'btn btn-danger'
        ]) */?>
    <?php /*$form->end(); */?>

<?php /*else: */?>

    <?php /*$form = ActiveForm::begin([
        'action' => Url::to(['/profile/settings/restore-account'])
    ]); */?>
        <?/*= Html::submitButton(Yii::t('profile', 'Restore Account'), [
            'class' => 'btn btn-success'
        ]) */?>
    <?php /*$form->end(); */?>

--><?php /*endif */?>


<section class="settings-panel">
    <div class="head">
        <div class="row">
            <div class="col-xs-6">
                Видалення облікового запису
            </div>
            <div class="col-xs-6 text-right">
                <a class="" role="button" data-toggle="collapse" href="#collapse4" aria-expanded="false" aria-controls="collapse4"></a>
            </div>
        </div>
    </div>
    <div class="panel-box">
        <div class="collapse" id="collapse4">
            <div class="well">
                <div class="row line">
                    <div class="col-xs-12">
                        <p class="desc-panel">Якщо Ви хочете назавжди видалити свій обліковий запис і всі свої оголошення, то натисніть на кнопку «Перейти до видалення облікового запису».</p>
                    </div>
                </div>
                <div class="row line">
                    <div class="col-xs-8">
                        <p><a class="btn btn-green" href="#" data-toggle="modal" data-target="#myModal">Перейти до видалення облікового запису</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
