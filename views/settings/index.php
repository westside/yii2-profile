<?php

/* @var $this      \yii\web\View */
/* @var $userModel \frontend\modules\profile\models\User */
/* @var $userDetailsModel \frontend\modules\profile\models\UserDetails */

$this->title = Yii::t('profile', 'Settings');
$this->params['breadcrumbs'][] = $this->title;
?>

<main class="content-holder">
    <?= $this->render('_form_main', compact('userDetailsModel')) ?>
    <?= $this->render('_form_details', compact('userDetailsModel')) ?>
    <?= $this->render('_form_password', compact('userModel')) ?>
    <?= $this->render('_form_notifications', compact('userDetailsModel')) ?>
    <?= $this->render('_form_delete') ?>
</main>


