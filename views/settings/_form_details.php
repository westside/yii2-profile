<?php

use yii\bootstrap\Html;
use yii\widgets\MaskedInput;
use yii\bootstrap\ActiveForm;
use vova07\fileapi\Widget as FileAPI;

/* @var $this \yii\web\View */
/* @var $userDetailsModel \frontend\modules\profile\models\UserDetails */
?>

<?php /*\yii\widgets\Pjax::begin() */?><!--
<?php /*$form = ActiveForm::begin([
    'layout' => 'horizontal',
]) */?>

    <?php /*if ($userDetailsModel->avatar_url): */?>
        <div class="form-group">
            <div class="col-sm-8 col-sm-offset-2 text-center">
                <?/*= Html::img($userDetailsModel->getAvatarUrl(), [
                    'class' => 'img-thumbnail',
                    'style' => 'margin-bottom: 20px',
                ]) */?>
            </div>
            <div class="col-sm-12 text-center">
                <?/*= Html::submitButton(Yii::t('profile', 'Update User Image'), ['class' => 'btn btn-success']) */?>
                <?/*= Html::a(Yii::t('profile', 'Delete'), \yii\helpers\Url::to(['/profile/settings/delete-avatar']), ['class' => 'btn btn-danger']) */?>
            </div>
        </div>
    <?php /*endif; */?>

    <?/*= $form->field($userDetailsModel, 'avatar_url')->widget(FileAPI::className(), [
        'crop' => true,
        'browseGlyphicon' => false,
        'jcropSettings' => [
            'aspectRatio' => 1,
            'bgColor' => '#ffffff',
            'maxSize' => [600, 600],
            'minSize' => [200, 200],
            'keySupport' => false, // Important param to hide jCrop radio button.
            'selection' => '100%'
        ],
        'settings' => [
            'url'  => ['/profile/settings/fileapi-upload'],
        ]
    ]) */?>

    <?/*= $form->field($userDetailsModel, 'location_id')->dropDownList([], [
        'prompt' => Yii::t('profile', 'Not Selected')
    ]) */?>

    <?/*= $form->field($userDetailsModel, 'first_name')->textInput() */?>
    <?/*= $form->field($userDetailsModel, 'last_name')->textInput() */?>
    <?/*= $form->field($userDetailsModel, 'phone')->widget(MaskedInput::className(), [
        'mask' => '+38 (099) 99-99-999',
        'options' => [
            'class' => 'form-control',
            'placeholder' => '+38 (0__) __-__-___',
        ],
    ]) */?>

    <?/*= $form->field($userDetailsModel, 'email_notifications')->checkbox() */?>
    <?/*= $form->field($userDetailsModel, 'event_notifications')->checkbox() */?>

    <div class="form-group">
        <div class="col-sm-6 col-sm-offset-3">
            <?/*= Html::submitButton(Yii::t('profile', 'Save Details'), ['class' => 'btn btn-primary']) */?>
        </div>
    </div>

<?php /*$form->end() */?>
--><?php /*\yii\widgets\Pjax::end() */?>

<section class="settings-panel">
    <div class="head">
        <div class="row">
            <div class="col-xs-6">
                Контактна інформація
            </div>
            <div class="col-xs-6 text-right">
                <a class="" role="button" data-toggle="collapse" href="#collapse1" aria-expanded="false" aria-controls="collapse1"></a>
            </div>
        </div>
    </div>
    <div class="panel-box">
        <div class="collapse" id="collapse1">
            <div class="well">
                <div class="row line">
                    <div class="col-xs-3">
                        <p>ПІБ</p>
                    </div>
                    <div class="col-xs-5">
                        <input class="form-control" value="Попов Сергій Іванович">
                        <div class="help-block help-block-error"></div>
                    </div>
                </div>
                <div class="row line">
                    <div class="col-xs-3">
                        <p>Країна</p>
                    </div>
                    <div class="col-xs-5">
                        <select class="selectpicker">
                            <option>Україна</option>
                            <option>Україна</option>
                            <option>Україна</option>
                        </select>
                        <div class="help-block help-block-error"></div>
                    </div>
                </div>
                <div class="row line">
                    <div class="col-xs-3">
                        <p>Місто</p>
                    </div>
                    <div class="col-xs-5">
                        <select class="selectpicker">
                            <option>Луцьк</option>
                            <option>Луцьк</option>
                            <option>Луцьк</option>
                        </select>
                        <div class="help-block help-block-error"></div>
                    </div>
                </div>
                <div class="row line">
                    <div class="col-xs-3">
                        <p>Телефон</p>
                    </div>
                    <div class="col-xs-5">
                        <input class="form-control" value="+38 095 48 58 414">
                        <div class="help-block help-block-error"></div>
                    </div>
                </div>
                <div class="row line">
                    <div class="col-xs-3">
                        <p></p>
                    </div>
                    <div class="col-xs-3">
                        <p><a class="btn btn-green" href="#">Зберегти</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
