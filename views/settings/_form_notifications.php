<section class="settings-panel">
    <div class="head">
        <div class="row">
            <div class="col-xs-6">
                Налаштування повідомлень
            </div>
            <div class="col-xs-6 text-right">
                <a class="" role="button" data-toggle="collapse" href="#collapse3" aria-expanded="false" aria-controls="collapse3"></a>
            </div>
        </div>
    </div>
    <div class="panel-box">
        <div class="collapse" id="collapse3">
            <div class="well">
                <div class="row line">
                    <div class="col-xs-12">
                        <p class="desc-panel">Позначте повідомлення, які Ви хочете отримувати по електронній пошті:</p>
                    </div>
                </div>
                <div class="row line">
                    <div class="col-xs-3">
                        <p>Новини сайту</p>
                    </div>
                    <div class="col-xs-5">
                        <div class="checkbox-b">
                            <input type="checkbox" class="check" id="my-checkbox1" name="check">
                            <label class="checkbox-label" for="my-checkbox1"></label>
                        </div>
                        <div class="help-block help-block-error"></div>
                    </div>
                </div>
                <div class="row line">
                    <div class="col-xs-3">
                        <p>Повідомлення</p>
                    </div>
                    <div class="col-xs-5">
                        <div class="checkbox-b">
                            <input type="checkbox" class="check" id="my-checkbox2" name="check">
                            <label class="checkbox-label" for="my-checkbox2"></label>
                        </div>
                        <div class="help-block help-block-error"></div>
                    </div>
                </div>
                <div class="row line">
                    <div class="col-xs-3">
                        <p>Нагадування</p>
                    </div>
                    <div class="col-xs-5">
                        <div class="checkbox-b">
                            <input type="checkbox" class="check" id="my-checkbox3" name="check">
                            <label class="checkbox-label" for="my-checkbox3"></label>
                        </div>
                        <div class="help-block help-block-error"></div>
                    </div>
                </div>
                <div class="row line">
                    <div class="col-xs-3">
                        <p></p>
                    </div>
                    <div class="col-xs-3">
                        <p><a class="btn btn-green" href="#">Зберегти</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>