<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
use westside\profile\models\Wallets;

/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $searchModel  \frontend\modules\profile\models\search\WalletsSearch */
?>

<h3 class="wallet-grey-block clearfix">
    <i class="wallet-article"></i>
    <?= implode(' ', [
        Yii::t('profile', 'User Balance'),
        Wallets::getUserBalance(),
        Yii::t('profile', 'UAH')
    ]) ?>
</h3>

<? Pjax::begin() ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{items}\n{summary}\n{pager}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'created_at',
                'value' => function($model) {
                    return Yii::$app->formatter->asDatetime($model->created_at);
                },
                'filter' => false,
            ],
            [
                'attribute' => 'payment_type_id',
                'value'     => function($model) {
                    return Wallets::getPaymentTypesText($model->payment_type_id);
                },
                'filter'    => Html::activeDropDownList(
                    $searchModel, 'payment_type_id',
                    Wallets::getPaymentTypes(),
                    ['prompt' => Yii::t('profile', '...'), 'class' => 'form-control']
                ),
            ],
            [
                'attribute' => 'payment_target_id',
                'value'     => function($model) {
                    return Wallets::getPaymentTargetsText($model->payment_target_id);
                },
                'filter'    => Html::activeDropDownList(
                    $searchModel, 'payment_target_id',
                    Wallets::getPaymentTargets(),
                    ['prompt' => Yii::t('profile', '...'), 'class' => 'form-control']
                ),
            ],
            [
                'attribute' => 'payment_system_id',
                'value'     => function($model) {
                    return Wallets::getPaymentSystemText($model->payment_system_id);
                },
                'filter'    => Html::activeDropDownList(
                    $searchModel, 'payment_system_id',
                    Wallets::getPaymentSystems(),
                    ['prompt' => Yii::t('profile', '...'), 'class' => 'form-control']
                ),
            ],
            [
                'attribute' => 'payment_value',
                'filter' => Html::activeTextInput($searchModel, 'downPrice', ['class' => 'form-control', 'placeholder' => 'From'])
                    . Html::activeTextInput($searchModel, 'upPrice', ['class' => 'form-control', 'placeholder' => 'To']),
                'value' => function($model) {
                    return implode(' ', [
                        $model->payment_value,
                        Yii::t('profile', 'UAH'),
                    ]);
                }
            ],
            [
                'attribute' => 'description',
                'filter'    => false,
            ],
        ],
    ]); ?>
<? Pjax::end() ?>
