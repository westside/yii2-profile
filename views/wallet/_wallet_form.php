<?php

use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use westside\profile\models\Wallets;

/* @var $this   \yii\base\View */
/* @var $wallet Wallets */
?>

<? $form = ActiveForm::begin([
    'action' => Url::to(['/profile/wallet/wallet-add']),
]) ?>

    <?= $form->field($wallet, 'payment_system_id')->dropDownList(Wallets::getPaymentSystems(), ['prompt' => '...']) ?>
    <?= $form->field($wallet, 'payment_value')->textInput() ?>
    <?= $form->field($wallet, 'description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('profile', 'Pay'), ['class' => 'btn btn-primary']) ?>
    </div>

<? ActiveForm::end() ?>

