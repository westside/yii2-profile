<?php

use yii\bootstrap\Modal;
use westside\profile\models\Wallets;

/* @var $this         \yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $wallet       \frontend\modules\profile\models\Wallets */
/* @var $searchModel  \frontend\modules\profile\models\search\WalletsSearch */

$modalAddMoneyId = 'walletAddMoney';
$modalAddMoneyOptions = [
    'id'           => $modalAddMoneyId,
    'header'       => '<h4 class="modal-title">' . Yii::t('profile', 'Add Money') . '</h4>',
    'toggleButton' => [
        'tag'   => 'a',
        'label' => Yii::t('profile', 'Add Money'),
        'class' => 'green-text',
    ],
];
$modalWalletHistoryOptions = [
    'header'       => '<h4 class="modal-title">' . Yii::t('profile', 'Wallet History') . '</h4>',
    'size'         => Modal::SIZE_LARGE,
    'toggleButton' => [
        'tag'   => 'a',
        'label' => Yii::t('profile', 'Wallet History'),
        'class' => 'link-underline',
    ],
];
?>

<main class="content-holder">
    <div class="wallet-grey-block clearfix">
            <span class="pull-left">
                <i class="wallet-article"></i>
                <?= Yii::t('profile', 'User Balance') ?>
                <span class="green-text">
                    <?= Wallets::getUserBalance() ?>
                    <?= Yii::t('profile', 'UAH') ?>
                </span>
            </span>
            <span class="pull-right">
                <? Modal::begin($modalAddMoneyOptions) ?>
                    <?= $this->render('_wallet_form', compact('wallet')) ?>
                <? Modal::end(); ?>
                <a href="#" class="link-underline"><?= Yii::t('profile', 'Service Payment') ?></a>
                <? Modal::begin($modalWalletHistoryOptions) ?>
                    <?= $this->render('_wallet_history', compact('dataProvider', 'searchModel')) ?>
                <? Modal::end(); ?>
            </span>
    </div>
    <div class="title-wallet">Поповнення гаманця</div>
    <div class="sum-wallet">
        <div>Сума:</div>
        <input id="paymentValue" type="text" value="<?= Wallets::PAYMENT_MINIMAL_VALUE ?>" class="form-control"/>
        Мінімальна сума <span class="red-text"><?= Wallets::PAYMENT_MINIMAL_VALUE ?> <?= Yii::t('profile', 'UAH') ?></span>
    </div>
    <div class="replenishment-card">
        <div class="title">Виберіть спосіб поповнення:</div>
        <div class="clearfix">
            <?php foreach (Wallets::getPaymentSystems() as $id => $paymentSystem): ?>
                <div class="one-rep">
                    <a href="#" data-toggle="modal" data-target="#<?= $modalAddMoneyId ?>" data-payment-system="<?= $id ?>">
                        <i class="ico-wallet ico-wallet1"></i>
                        <?= $paymentSystem ?>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</main>

<?php $this->registerJs('
    $(\'#walletAddMoney\').on(\'show.bs.modal\', function (event) {
        $(this).find(\'.modal-body select[name*="payment_system_id"]\')
            .val($(event.relatedTarget)
                .data(\'payment-system\'))
        $(this).find(\'.modal-body input[name*="payment_value"]\')
            .val($(\'#paymentValue\')
                .val())
    })
') ?>