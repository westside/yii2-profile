<?php

use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use westside\profile\models\Wallets;

/* @var $this         \yii\web\View */
/* @var $wallet       Wallets */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $searchModel  \frontend\modules\profile\models\search\WalletsSearch */

$this->title = Yii::t('profile', 'Wallet');
$this->params['breadcrumbs'][] = $this->title;

$modalAddMoneyId = 'walletAddMoney';
$modalAddMoneyOptions = [
    'id'           => $modalAddMoneyId,
    'size'         => Modal::SIZE_DEFAULT,
    'header'       => '<h4 class="modal-title">'.Yii::t('profile', 'Add Money').'</h4>',
    'toggleButton' => [
        'tag'   => 'a',
        'label' => Yii::t('profile', 'Add Money'),
    ],
];
$modalWalletHistoryOptions = [
    'size'         => Modal::SIZE_LARGE,
    'header'       => '<h4 class="modal-title">'.Yii::t('profile', 'Wallet History').'</h4>',
    'toggleButton' => [
        'tag'   => 'a',
        'label' => Yii::t('profile', 'Wallet History'),
    ],
];
?>

<div class="row">
    <div class="col-sm-12 col-xs-12">
        <div class="pull-left">
            <h1><?= $this->title ?></h1>
            <?= Yii::t('profile', 'User Balance') ?>
            <?= Wallets::getUserBalance() ?>
            <?= Yii::t('profile', 'UAH') ?>
        </div>
        <div class="pull-right">
            <? Modal::begin($modalAddMoneyOptions) ?>
                <?= $this->render('_wallet_form',
                    compact('wallet')) ?>
            <? Modal::end(); ?>
            <? Modal::begin($modalWalletHistoryOptions) ?>
                <?= $this->render('_wallet_history',
                    compact('dataProvider', 'searchModel')) ?>
            <? Modal::end(); ?>
        </div>
    </div>

    <div class="col-sm-12 col-xs-12">
        <h3><?= Yii::t('profile', 'Add Money') ?></h3>
    </div>

    <div class="col-sm-5 col-xs-12">
        <div class="form-group">
            <?= Html::label(Yii::t('profile', 'Amount'))?>
            <?= Html::textInput('amount', Wallets::PAYMENT_MINIMAL_VALUE, [
                'class' => 'form-control',
                'id' => 'paymentValue',
            ]) ?>
            <span class="help-block">
                <?= Yii::t('profile', 'Minimal Payment Amount') ?>
                <?= Wallets::PAYMENT_MINIMAL_VALUE ?>
                <?= Yii::t('profile', 'UAH') ?>
            </span>
        </div>
    </div>

    <div class="col-sm-5 col-sm-offset-2 col-xs-12">
        <h4><?= Yii::t('profile', 'Choose Payment System') ?>:</h4>
        <div class="clearfix">
            <?php foreach (Wallets::getPaymentSystems() as $id => $paymentSystem): ?>
                <div class="one-rep">
                    <a href="#" data-toggle="modal" data-target="#<?= $modalAddMoneyId ?>" data-payment-system="<?= $id ?>">
                        <i class="ico-wallet ico-wallet1"></i>
                        <?= $paymentSystem ?>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>


<?php $this->registerJs('
    $(\'#walletAddMoney\').on(\'show.bs.modal\', function (event) {
        $(this).find(\'.modal-body select[name*="payment_system_id"]\')
            .val($(event.relatedTarget)
                .data(\'payment-system\'))
        $(this).find(\'.modal-body input[name*="payment_value"]\')
            .val($(\'#paymentValue\')
                .val())
    })
') ?>