<?php

use yii\helpers\Url;

/** @var $this  \yii\web\View */
/** @var $model \frontend\modules\profile\models\Messages */

$readMoreLink = Url::to(['/profile/dialogs/messages', 'id' => $model->dialog->id]);
$userAvatar   = implode('', [
    'http://dummyimage.com/60x60/555/fff&text=',
    $model->userFrom->email,
]);
?>

<?php if ($model->userFrom->id != Yii::$app->user->id): ?>

    <div class="media">
        <div class="media-left messages">
            <a href="<?= $readMoreLink ?>">
                <img class="media-object" src="<?= $userAvatar ?>" alt="<?= $model->userFrom->email ?>">
            </a>
        </div>
        <div class="media-body dialog">
            <?php if ($model->dialog->title): ?>
                <h4 class="media-heading">
                    <a href="#"><span><?= Yii::t('app', 'Subject') ?>:</span> <?= $model->dialog->title ?></a>
                </h4>
            <?php endif; ?>
            <span><?= Yii::t('app', 'From') ?>: <?= $model->userFrom->email ?></span>
            <blockquote>
                <p><?= $model->message ?></p>
                <p><cite title="<?= Yii::t('app', 'At') ?>"><?= Yii::$app->formatter->asDatetime($model->created_at) ?></cite></p>
            </blockquote>
        </div>
    </div>

<?php else: ?>

    <div class="media">
        <div class="media-body second">
            <?php if ($model->dialog->title): ?>
                <h4 class="media-heading">
                    <a href="#"><span><?= Yii::t('app', 'Subject') ?>:</span> <?= $model->dialog->title ?></a>
                </h4>
            <?php endif; ?>
            <span><?= Yii::t('app', 'From') ?>: <?= $model->userFrom->email ?></span>
            <blockquote class="blockquote-reverse">
                <p><?= $model->message ?></p>
                <p><cite title="<?= Yii::t('app', 'At') ?>"><?= Yii::$app->formatter->asDatetime($model->created_at) ?></cite></p>
            </blockquote>
        </div>
        <div class="media-right messages">
            <a href="<?= $readMoreLink ?>">
                <img class="media-object" src="<?= $userAvatar ?>" alt="<?= $model->userFrom->email ?>">
            </a>
        </div>
    </div>

<?php endif; ?>