<?php

use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/** @var $this            \yii\web\View */
/** @var $dialog          \frontend\modules\profile\models\Dialogs */
/** @var $messageModel    \frontend\modules\profile\models\Messages */
/** @var $dataProvider    \yii\data\ActiveDataProvider */
/** @var $searchModel     \frontend\modules\profile\models\search\DialogsSearch */

$this->title = Yii::t('dialogs', 'Dialog with') . ': ' . $dialog->dialogCompanion->email;
$this->params['breadcrumbs'][] = ['label' => Yii::t('dialogs', 'Dialogs'), 'url' => ['/dialogs']];
$this->params['breadcrumbs'][] = $this->title;

$formSendMessageOptions = [
    'action' => Url::to(['/dialogs/default/message-send'])
];
$modalSendMessageOptions = [
    'id'           => 'modalSendMessage',
    'options' => [
        'role' => 'dialog',
        'class' => 'text-left',
    ],
    'header'       => '<h4>' . Yii::t('dialogs', 'Write New Message') . '</h4>',
    'toggleButton' => [
        'tag'   => 'a',
        'label' => Yii::t('dialogs', 'Write New Message'),
        'class' => 'btn btn-default',
    ],
];
$messagesListOptions = [
    'dataProvider' => $dataProvider,
    'itemView'     => function($model, $key, $index, $widget){
        return $this->render('_message_item', compact('model'));
    },
    'pager'       => [
        'options' => [
            'class' => 'pagination center-block'
        ],
    ],
    'options'     => [
        'tag' => 'section',
        'class' => 'messages-box',
        'id' => false,
    ],
    'layout'      => "{items}\n{pager}",
    'itemOptions' => [
        'tag'   => 'div',
        'class' => 'messages-block dialog',
    ],
];
?>

<?php $this->registerJs('
    setInterval(function(){
        if (!$(\'#modalSendMessage\').hasClass(\'in\')){
            $.pjax.reload({container:\'#messagesList\'});
        }
    }, 3000);
'); ?>

<main class="content-holder">
    <div class="send-message-box text-right">
        <?php Modal::begin($modalSendMessageOptions); ?>
            <?php $form = ActiveForm::begin($formSendMessageOptions); ?>
                <?= Html::activeHiddenInput($messageModel, 'user_to_id', ['value' => $dialog->dialogCompanion->id]) ?>
                <?= $form->field($messageModel, 'message')->textarea(['rows' => '4']) ?>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('dialogs', 'Send'), ['class' => 'btn btn-green-gradient']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        <?php Modal::end(); ?>
    </div>
    <? Pjax::begin(['id' => 'messagesList'])?>
        <?= ListView::widget($messagesListOptions)?>
    <?php Pjax::end() ?>
</main>