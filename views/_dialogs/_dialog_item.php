<?php

use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\helpers\StringHelper;
use westside\profile\models\Dialogs;
use westside\profile\models\Messages;

/** @var $model Messages */
$newMessages      = null;
$newMessagesCount = false;
if ($model->dialog->status_id == Dialogs::DIALOG_STATUS_NEW)
    if ($model->userTo->id == Yii::$app->user->id)
    {
        $newMessages = 'new-message';
        $newMessagesCount = $model->dialog
            ->getMessages()
            ->where(['user_to'   => Yii::$app->user->id ])
            ->where(['status_id' => Messages::MESSAGE_STATUS_NEW])
            ->count();
    }

$readMoreLink = Url::to(['/dialogs/default/messages', 'id' => $model->dialog->id]);
$userAvatar   = implode('', [
    'http://dummyimage.com/80x80/555/fff&text=',
    $model->dialog->dialogCompanion->email,
])
?>

<div class="media">
    <div class="media-left messages">
        <a href="<?= $readMoreLink ?>">
            <img class="media-object" src="<?= $userAvatar ?>" alt="">
        </a>
    </div>
    <div class="media-body <?= $newMessages ?>">
        <?php if ($model->dialog->title): ?>
            <h4 class="media-heading">
                <a href="#"><span><?= Yii::t('dialogs', 'Subject') ?>:</span> <?= $model->dialog->title ?></a>
            </h4>
        <?php endif; ?>
        <span><?= Yii::t('dialogs', 'From') ?>: <?= $model->userFrom->email ?></span>
        <blockquote>
            <p><?= StringHelper::truncateWords($model->message, 25) ?></p>
            <p><cite title="<?= Yii::t('dialogs', 'Created At') ?>">
                    <?= Yii::$app->formatter->asDate($model->created_at) ?>
                    <?= Yii::t('dialogs', 'At') ?>
                    <?= Yii::$app->formatter->asTime($model->created_at) ?>
                </cite></p>
        </blockquote>
        <?= Html::a(
            '<i class="fa fa-eye"></i>' . Yii::t('dialogs', 'Read More'),
            $readMoreLink,
            ['class' => 'btn btn-green-gradient pull-right']
        ) ?>
    </div>
</div>