<?php

use yii\helpers\Html;
use common\helpers\Price;
use common\helpers\Currency;

/* @var $this yii\web\View */
/* @var $model common\models\Ad */

$now = new DateTime();
$createdAt = new DateTime();
$createdAt->setTimestamp($model->created_at);
$daysLeft = date_diff($now, $createdAt);
$daysLeft = 30 - date_interval_format($daysLeft, "%d");
$percentLeft = floor(($daysLeft / 30) * 100)
?>

<div class="box-article">
    <div class="clearfix">
        <input type="checkbox"/>
        <div class="box-img">
            <?php if ($model->photos): ?>
                <?= Html::img($model->photos[0]->url, ['alt' => $model->title])?>
            <?php else: ?>
                <?= Html::img('/content/ad/default.png', ['alt' => $model->title])?>
            <?php endif; ?>
        </div>
        <div class="box-text">
            <div>
                <?= Html::a(
                    $model->title,
                    ['/ad/view', 'id' => $model->id, 'slug' => $model->slug],
                    ['target' => '_blank', 'class' => 'title']
                )?>
            </div>
            <div class="nomer"><?= Yii::t('profile', 'Ad Number') ?>: <?= $model->id ?></div>
            <div class="progress">
                <div class="progress-bar progress-bar-success" role="progressbar"
                     aria-valuenow="<?= $percentLeft ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= $percentLeft ?>%">
                    <span class="sr-only"><?= $percentLeft ?>% Complete (success)</span>
                </div>
            </div>
            <div class="last-time">
                <?= Yii::t('profile', 'Days Left') ?>
                <?= $daysLeft ?> д -
                <span class="red-text"><?= Yii::t('profile', 'Published At') ?>
                    <?= date('H:i d.m.Y', $model->created_at)?>
                </span>
            </div>
            <div class="btn-block">
                <?= Html::a(
                    '<i class="fa fa-fw"></i>'.Yii::t('profile', 'View'),
                    ['/ad/view', 'id' => $model->id, 'slug' => $model->slug],
                    ['target' => '_blank']
                )?>
                <?= Html::a(
                    '<i class="fa fa-fw"></i>'.Yii::t('profile', 'Edit'),
                    ['/ad/edit', 'id' => $model->id, 'slug' => $model->slug],
                    ['target' => '_blank']
                )?>

                <?php if ($model->status === \common\models\Ad::AD_STATUS_ACTIVE): ?>
                    <?= Html::a(
                        '<i class="fa fa-fw"></i>'.Yii::t('profile', 'Cancel'),
                        ['/ad/deactivate', 'id' => $model->id],
                        ['class' => 'danger']
                    )?>
                <?php elseif ($model->status === \common\models\Ad::AD_STATUS_INACTIVE): ?>
                    <?= Html::a(
                        '<i class="fa fa-fw"></i>'.Yii::t('profile', 'Activate'),
                        ['/ad/activate', 'id' => $model->id]
                    )?>
                <?php endif; ?>
            </div>
        </div>
        <div class="price-box">
            <div class="price">
                <?= Price::formatPrice($model->price)?>
                <?= Currency::getCurrencyText($model->currency)?>
            </div>
            <?= Html::a(
                Yii::t('profile', 'Advertise'),
                ['/ad/payment', 'id' => $model->id, 'slug' => $model->slug],
                ['class' => 'btn btn-green']
            )?>
        </div>
    </div>
    <div class="statistics clearfix">
        <div class="word"><?= Yii::t('profile', 'Statistic') ?>:</div>
        <ul>
            <li><i class="fa fa-fw"></i><?= Yii::t('profile', 'Views') ?>: 0</li>
            <li><i class="fa fa-fw"></i><?= Yii::t('profile', 'Phone') ?>: 0</li>
            <li><i class="fa fa-fw"></i><?= Yii::t('profile', 'Messages') ?>: 0</li>
            <li><i class="fa fa-fw"></i><?= Yii::t('profile', 'In Favourites') ?>: 0</li>
            <li><i class="fa fa-fw"></i><?= Yii::t('profile', 'Skype') ?>: 0</li>
        </ul>
    </div>
</div>