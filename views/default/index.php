<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\AdSearch */
/* @var $dataProviderActive yii\data\ActiveDataProvider */
/* @var $dataProviderCompleted yii\data\ActiveDataProvider */

$this->title = Yii::t('profile', 'My Ads');
/*$this->params['breadcrumbs'][] = $this->title;*/

$listViewActiveOptions = [
    'dataProvider' => $dataProviderActive,
    'layout' => "{items}\n{pager}",
    'itemView' => '_item_ad',
    'itemOptions' => false,
];

$listViewCompletedOptions = [
    'dataProvider' => $dataProviderCompleted,
    'layout' => "{items}\n{pager}",
    'itemView' => '_item_ad',
    'itemOptions' => false,
];

$orderListItems = [
    'byDate' => Yii::t('profile', 'byDate'),
    'byViews' => Yii::t('profile', 'byView'),
    'byRating' => Yii::t('profile', 'byRating'),
];
?>

<main class="content-holder">
    <section class="items-profile-section">
        <ul class="nav nav-tabs messages" role="tablist">
            <li role="presentation" class="active">
                <a href="#m1" aria-controls="m1" role="tab" data-toggle="tab">
                    <?= Yii::t('profile', 'Active') ?>
                </a>
            </li>
            <li role="presentation" class="">
                <a href="#m2" aria-controls="m2" role="tab" data-toggle="tab">
                    <?= Yii::t('profile', 'Completed') ?>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="m1">
                <section class="profile-item-box">
                    <div class="list-block-article">
                        <div class="top clearfix">
                            <span class="pull-left">
                                <input type="checkbox"/> <?= Yii::t('profile', 'Select All') ?>
                            </span>
                            <span class="pull-right">
                                <?= Yii::t('profile', 'Order') ?>:
                                <?= \yii\helpers\Html::dropDownList('order', null, $orderListItems, ['class' => 'appearance-none'])?>
                            </span>
                        </div>
                        <?= ListView::widget($listViewActiveOptions) ?>
                    </div>
                    <div class="profile-item-block">
                    </div>
                </section>
            </div>
            <div role="tabpanel" class="tab-pane" id="m2">
                <section class="profile-item-box">
                    <div class="list-block-article">
                        <div class="top clearfix">
                            <span class="pull-left">
                                <input type="checkbox"/> <?= Yii::t('profile', 'Select All') ?>
                            </span>
                            <span class="pull-right">
                                <?= Yii::t('profile', 'Order') ?>:
                                <?= Html::dropDownList('order', null, $orderListItems, ['class' => 'appearance-none'])?>
                            </span>
                        </div>
                        <?= ListView::widget($listViewCompletedOptions) ?>
                    </div>
                    <div class="profile-item-block">
                    </div>
                </section>
            </div>
        </div>
    </section>
</main>
