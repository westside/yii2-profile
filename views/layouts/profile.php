<?php

use yii\widgets\Menu;
use yii\bootstrap\Html;
use yii\widgets\Breadcrumbs;
use frontend\widgets\BannerWidget;
use westside\profile\models\Wallets;
use westside\profile\models\Messages;

/* @var $this    \yii\web\View */
/* @var $content string */

$badgeNewMessages = Messages::getCountNewMessages() != 0
    ? '<span class="messages-numb">' . Messages::getCountNewMessages() . '</span>' : null;

$this->beginContent('@frontend/views/layouts/header-big.php') ?>

        <section>
            <div class="row">
                <div class="col-xs-12">
                    <nav class="bottom-menu">
                        <ul class="menu-left">
                            <li>
                                <?= Yii::t('profile', 'Search In Region') ?>
                                <a href="#"><?= Yii::t('profile', 'All Regions') ?></a>
                            </li>
                        </ul>
                        <ul class="menu-right">
                            <li><a href="#"><?= Yii::t('profile', 'All ads') ?></a></li>
                            <li><a href="#"><?= Yii::t('profile', 'Shops') ?></a></li>
                            <li class="divider"><a href="#"><?= Yii::t('profile', 'Support') ?></a></li>
                            <li><a href="#"><?= Yii::t('profile', 'Site Map') ?></a></li>
                        </ul>
                    </nav>
                    <div class="search-menu">
                        <div class="col-xs-3">
                            <?= Html::dropDownList('categories', null, [], [
                                'class'  => 'selectpicker',
                                'prompt' => Yii::t('profile', 'Default Value'),
                            ]) ?>
                        </div>
                        <div class="col-xs-7">
                            <input class="form-control" placeholder="<?= Yii::t('profile', 'Search Ads') ?>">
                        </div>
                        <div class="col-xs-2">
                            <a href="#" class="btn btn-default"><?= Yii::t('profile', 'Search') ?></a>
                        </div>
                    </div>
                    <?= BannerWidget::widget(['type' => 1])?>
                </div>
            </div>
        </section>

        <article>
            <div class="row">
                <div class="col-xs-3">
                    <section class="left-menu-box">
                        <nav class="left-nav">
                            <?= Menu::widget([
                                'options' => ['class' => 'li-group'],
                                'activateItems' => true,
                                'encodeLabels' => false,
                                'items'   => [
                                    [
                                        'label' => '<i class="fa fa-bell"></i>'.Yii::t('profile', 'Events List'),
                                        'url'   => ['/profile/default/not-ready'],
                                        'options' => ['class' => 'divider'],
                                    ],
                                    [
                                        'label' => '<i class="fa fa-bookmark"></i>'.Yii::t('profile', 'My Ads'),
                                        'url'   => ['/profile/default/index'],
                                    ],
                                    [
                                        'label' => '<i class="fa fa-shopping-cart"></i>'.Yii::t('profile', 'Shops'),
                                        'url'   => ['/profile/default/not-ready'],
                                    ],
                                    [
                                        'label' => '<i class="fa fa-heart"></i>'.Yii::t('profile', 'Selected Ads'),
                                        'url'   => ['/profile/default/not-ready'],
                                        'options' => ['class' => 'divider'],
                                    ],
                                    [
                                        'label' => '<i class="fa fa-user"></i>'.Yii::t('profile', 'Friends'),
                                        'url'   => ['/profile/default/not-ready'],
                                    ],
                                    [
                                        'label' => '<i class="fa fa-envelope"></i>'.Yii::t('profile', 'Messages')
                                            .$badgeNewMessages,
                                        'url'   => ['/profile/dialogs/index'],
                                    ],
                                    [
                                        'label' => '<i class="fa fa-credit-card-alt"></i>'
                                                    .Yii::t('profile', 'Wallet')
                                                    .' ('. Wallets::getUserBalance() . ' '.Yii::t('profile', 'UAH').')',
                                        'url'   => ['/profile/wallet/index'],
                                    ],
                                    [
                                        'label' => '<i class="fa fa-camera"></i>'.Yii::t('profile', 'Media'),
                                        'url'   => ['/profile/default/not-ready'],
                                    ],
                                    [
                                        'label' => '<i class="fa fa-file-text"></i>'.Yii::t('profile', 'Documents'),
                                        'url'   => ['/profile/default/not-ready'],
                                    ],
                                    [
                                        'label' => '<i class="fa fa-users"></i>'.Yii::t('profile', 'Communities'),
                                        'url'   => ['/profile/default/not-ready'],
                                    ],
                                    [
                                        'label' => '<i class="fa fa-cogs"></i>'.Yii::t('profile', 'Settings'),
                                        'url'   => ['/profile/settings'],
                                        'options' => ['class' => 'divider'],
                                    ],
                                    [
                                        'label' => '<i class="fa fa-comments"></i>'.Yii::t('profile', 'Support'),
                                        'url'   => ['/profile/default/not-ready'],
                                        'options' => ['class' => 'divider'],
                                    ],
                                    [
                                        'label' => '<i class="fa fa-sign-out"></i>'.Yii::t('profile', 'Exit'),
                                        'url'   => ['/'],
                                        'template' => Html::a('{label}', '{url}'),
                                    ],
                                ],
                            ]) ?>
                        </nav>
                        <?= BannerWidget::widget(['type' => BannerWidget::BANNER_SIDE])?>
                    </section>
                </div>
                <div class="col-xs-9">
                    <article id="content">
                        <section class="content-head">
                            <div class="content-title"><?= $this->title ?></div>
                            <div class="breadcrumb inline">
                                <?= Breadcrumbs::widget([
                                    'itemTemplate' => "<li>{link}</li>\n",
                                    'links'        => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                                ]); ?>
                            </div>
                        </section>

                        <?= $content ?>

                    </article>
                </div>
            </div>
        </article>

    <?php $this->endContent() ?>