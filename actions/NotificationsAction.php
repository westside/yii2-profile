<?php

namespace westside\profile\actions;

use Yii;
use yii\base\Action;
use westside\profile\models\MessageNotifications;

/**
 * Class NotificationsAction
 * @package frontend\modules\profile\actions
 */
class NotificationsAction extends Action
{
    /**
     * @inheritdoc
     * @return array
     */
    public function run()
    {
        $messages = [];
        Yii::$app->response->format = 'json';

        $notifications = MessageNotifications::find()
            ->andWhere(['user_id'   => Yii::$app->user->id])
            ->andWhere(['status_id' => 0])
            ->all();

        foreach ($notifications as $notification)
        {
            $messages[] = [
                'type'    => 'info',
                'title'   => Yii::t('profile', 'New Message'),
                'message' => $notification->text,
            ];
            $notification->status_id = 1;
            $notification->save(false);
        }

        return $messages;
    }
}