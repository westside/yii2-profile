<?php

namespace westside\profile\helpers;

use Yii;
use yii\helpers\ArrayHelper;
use westside\profile\models\User;

/**
 * Class Users
 * @package frontend\modules\dialogs\helpers
 */
class UsersHelper
{
    /**
     * @return array
     */
    public static function getExcludeCurrent()
    {
        return ArrayHelper::map(
            User::find()->filterWhere(['!=', 'id', Yii::$app->user->id])->all(),
            'id', 'email'
        );
    }
}