<?php

use yii\db\Migration;

class m160406_130119_add_avatar_url_to_user_details extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user_details}}', 'avatar_url', $this->string()->defaultExpression('NULL'));
    }

    public function down()
    {
        $this->dropColumn('{{%user_details}}', 'avatar_url');
    }
}
