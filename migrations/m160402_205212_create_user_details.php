<?php

use yii\db\Migration;

class m160402_205212_create_user_details extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_details}}', [
            'user_id' => $this->primaryKey(),

            'location_id' => $this->integer(),
            'first_name' => $this->string(255),
            'last_name' => $this->string(255),
            'phone' => $this->string(255),

            'email_notifications' => $this->boolean()->notNull()->defaultValue(0),
            'event_notifications' => $this->boolean()->notNull()->defaultValue(0),
        ], $tableOptions);

        $this->addForeignKey('fk_user_details', '{{%user_details}}', 'user_id', '{{%user}}', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_user_details', '{{%user_details}}');
        $this->dropTable('user_details');
    }
}
