<?php

use yii\db\Migration;

class m160318_152554_create_message_notifications extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%message_notifications}}', [
            'id' => $this->primaryKey(),

            'user_id'    => $this->integer()->notNull(),
            'message_id' => $this->integer()->notNull(),
            'type_id'    => $this->smallInteger()->notNull()->defaultValue(0),

            'text' =>$this->text()->notNull(),

            'status_id'  => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('fk_notifications_users',   '{{%message_notifications}}', 'user_id',    '{{%user}}',    'id');
        $this->addForeignKey('fk_notifications_message', '{{%message_notifications}}', 'message_id', '{{%messages}}', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('fk_notifications_users',   '{{%message_notifications}}');
        $this->dropForeignKey('fk_notifications_message', '{{%message_notifications}}');
        $this->dropTable('{{%message_notifications}}');
    }
}
