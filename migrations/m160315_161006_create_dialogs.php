<?php

use yii\db\Migration;

class m160315_161006_create_dialogs extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%dialogs}}', [
            'id' => $this->primaryKey(),

            'user_to_id'   => $this->integer()->notNull(),
            'user_from_id' => $this->integer()->notNull(),

            'dialog_type_id' => $this->smallInteger()->notNull()->defaultValue(0),
            'title'          => $this->string(255)->defaultExpression('NULL'),

            'status_id'  => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('fk_dialogs_users_to',   '{{%dialogs}}', 'user_to_id',   '{{%user}}', 'id');
        $this->addForeignKey('fk_dialogs_users_from', '{{%dialogs}}', 'user_from_id', '{{%user}}', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('fk_dialogs_users_to',   '{{%dialogs}}');
        $this->dropForeignKey('fk_dialogs_users_from', '{{%dialogs}}');

        $this->dropTable('{{%dialogs}}');
    }
}
