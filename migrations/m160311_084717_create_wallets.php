<?php

use yii\db\Migration;

class m160311_084717_create_wallets extends Migration
{
    public function up()
    {
        $this->createTable('{{%wallets}}', [
            'id' => $this->primaryKey(),

            'user_id' => $this->integer()->notNull(),
            'shop_id' => $this->integer()->defaultExpression('NULL'),

            'payment_type_id'   => $this->smallInteger()->notNull()->defaultValue(0),
            'payment_target_id' => $this->smallInteger()->notNull()->defaultValue(0),
            'payment_system_id' => $this->smallInteger()->notNull()->defaultValue(0),
            'payment_value'     => $this->money(8,2)->notNull(),
            'description'       => $this->text()->defaultExpression('NULL'),

            'created_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk_wallets_users', '{{%wallets}}', 'user_id', '{{%user}}', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('fk_wallets_users', '{{%wallets}}');
        $this->dropTable('{{%wallets}}');
    }
}
