<?php

use yii\db\Migration;

class m160315_161017_create_messages extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%messages}}', [
            'id'        => $this->primaryKey(),
            'dialog_id' => $this->integer()->notNull(),

            'user_to_id'   => $this->integer()->notNull(),
            'user_from_id' => $this->integer()->notNull(),

            'message' => $this->text()->notNull(),

            'status_id' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('fk_messages_dialogs',    '{{%messages}}', 'dialog_id',    '{{%dialogs}}', 'id');
        $this->addForeignKey('fk_messages_users_to',   '{{%messages}}', 'user_to_id',   '{{%user}}',   'id');
        $this->addForeignKey('fk_messages_users_from', '{{%messages}}', 'user_from_id', '{{%user}}',   'id');
    }

    public function down()
    {
        $this->dropForeignKey('fk_messages_dialogs',    '{{%messages}}');
        $this->dropForeignKey('fk_messages_users_to',   '{{%messages}}');
        $this->dropForeignKey('fk_messages_users_from', '{{%messages}}');
        $this->dropTable('{{%messages}}');
    }
}
