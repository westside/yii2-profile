<?php

use yii\db\Migration;

class m160315_164704_create_message_attachments extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%message_attachments}}', [
            'id'         => $this->primaryKey(),
            'message_id' => $this->integer()->notNull(),

            'url'     => $this->string('255')->notNull(),
            'title'   => $this->string('255')->defaultExpression('NULL'),
            'type_id' => $this->smallInteger()->notNull()->defaultValue(0),
        ], $tableOptions);

        $this->addForeignKey('fk_message_attachments', '{{%message_attachments}}', 'message_id', '{{%messages}}', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('fk_message_attachments', '{{%message_attachments}}');
        $this->dropTable('{{%message_attachments}}');
    }
}
