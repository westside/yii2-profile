<?php

namespace westside\profile\components;

use Yii;
use yii\base\Event;
use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\db\ActiveRecord;
use yii\helpers\StringHelper;
use yii\base\BootstrapInterface;
use westside\profile\models\Messages;
use westside\profile\models\MessageNotifications;

class ToastrEvents implements BootstrapInterface
{
    /**
     * @inheritdoc
     * @param \yii\base\Application $app
     */
    public function bootstrap($app)
    {
        Event::on(
            Messages::className(),
            ActiveRecord::EVENT_AFTER_INSERT,
            [$this, 'newMessage']
        );
    }

    /**
     * @param Event $event
     */
    public function newMessage(Event $event)
    {
        /** @var Messages $message*/
        $message = $event->sender;

        $notification = new MessageNotifications();
        $notification->user_id    = $message->userTo->id;
        $notification->message_id = $message->id;
        $notification->text = '<div>' . Yii::t('profile', 'From') . ': '
                               . $message->userFrom->email . ' </div>'
                               . '<p>' . StringHelper::truncateWords($message->message, 10) . '</p>'
                               . Html::a(
                                    Yii::t('app', 'Read More'),
                                    Url::to(['/dialogs/default/messages', 'id' => $message->dialog_id]),
                                    ['class' => 'btn btn-info pull-right']
                               );
        $notification->save();
    }
}